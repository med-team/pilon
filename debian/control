Source: pilon
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Pierre Gruet <pgt@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               javahelper,
               scala,
               libhtsjdk-java
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/pilon
Vcs-Git: https://salsa.debian.org/med-team/pilon.git
Homepage: https://github.com/broadinstitute/pilon/wiki
Rules-Requires-Root: no

Package: pilon
Architecture: all
Depends: ${misc:Depends},
         default-jre,
         java-wrappers,
         libhtsjdk-java,
         libcommons-lang-java,
         scala-library
Conflicts: pilon-non-free
Replaces: pilon-non-free
Description: automated genome assembly improvement and variant detection tool
 Pilon is a software tool which can be used to:
  * Automatically improve draft assemblies
  * Find variation among strains, including large event detection
 Pilon requires as input a FASTA file of the genome along with one or more
 BAM files of reads aligned to the input FASTA file. Pilon uses read
 alignment analysis to identify inconsistencies between the input genome and
 the evidence in the reads. It then attempts to make improvements to the
 input genome, including:
  * Single base differences
  * Small indels
  * Larger indel or block substitution events
  * Gap filling
  * Identification of local misassemblies, including optional opening
    of new gaps
